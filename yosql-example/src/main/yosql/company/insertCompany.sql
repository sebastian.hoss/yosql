--
-- parameters:
--   - name: id
--     type: int
--   - name: name
--     type: java.lang.String
--
INSERT INTO companies (id, name)
    VALUES (:id, :name)
;
